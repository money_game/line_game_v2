class BallSprite extends egret.Sprite {

    public column: number;
    public row: number;
    public color: BallColor;
    public armatureDisplay: dragonBones.EgretArmatureDisplay;
    public isActive: boolean;
    private static DEACTIVE_SCALE: number = 0.3;
    private static ACTIVE_SCALE: number = 0.9;
    
    public constructor() {
        super();
        this.init(BallColor.BLUE);
        this.addChild(this.armatureDisplay);
    }

    public init(color: BallColor): void {
        this.color = color;
        this.armatureDisplay = ArmatureUtil.getInstance().getArmatureDisplay(this.color.toString());
        if (!this.armatureDisplay) {
            this.armatureDisplay;
        }
        this.armatureDisplay.scaleX = BallSprite.DEACTIVE_SCALE;
        this.armatureDisplay.scaleY = BallSprite.DEACTIVE_SCALE;
        this.removeChildren();
        this.addChild(this.armatureDisplay);
        this.show();
    }

    public setActive(isActive: boolean): void {
        this.isActive = isActive;
        this.touchEnabled = this.isActive;
        if (this.isActive) {
            this.armatureDisplay.scaleX = BallSprite.ACTIVE_SCALE;
            this.armatureDisplay.scaleY = BallSprite.ACTIVE_SCALE;
        } else {
            this.armatureDisplay.scaleX = BallSprite.DEACTIVE_SCALE;
            this.armatureDisplay.scaleY = BallSprite.DEACTIVE_SCALE;
        }
    }

    public setIndex(row: number, column: number): void {
        this.row    = row;
        this.column = column;
    }

    public show(): void {
        this.visible = true;
    }

    public hide(): void {
        this.visible = false;
    }

    public runAnimHide() {
        egret.Tween.get(this.armatureDisplay, {loop: false})
        .to({scaleX: 0, scaleY: 0}, 100 )
        .call(()=>{this.hide();}, this);
    }
}