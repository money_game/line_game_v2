class SkillItem {

    button: eui.Button;
    count: number;
    state: number;
    onSelected: Function;

    constructor(btn: eui.Button, count, cb: Function) {

        this.button = btn;
        this.setTouchEnabled(true);
        if (this.button.hasEventListener(egret.TouchEvent.TOUCH_END)) {
            this.button.removeEventListener(egret.TouchEvent.TOUCH_END, this.onTouchButton, this);
        }
        this.button.addEventListener(egret.TouchEvent.TOUCH_END, this.onTouchButton, this);

        this.count = count;
        this.onSelected = cb;
        this.updateState();
    }

    private onTouchButton(event: egret.TouchEvent): void {
        this.touch();
    }

    private setState(state: number): void {
        this.state = state;
        switch(this.state) {
            case GameDefine.SKILL_ITEM_STATE_ACTIVE:{
                this.setTouchEnabled(true);
                this.button.enabled = true;
                break;
            }
            case GameDefine.SKILL_ITEM_STATE_DEACTIVE:{
                this.button.enabled = false;
                break;
            }
            case GameDefine.SKILL_ITEM_STATE_SELECTED:{
                break;
            }
        }
    }

    public updateState(): void {
        if (this.count > 0) {
            this.active();
        } else {
            this.deactive();
        }
    }

    private touch(): void {
        switch(this.state) {
            case GameDefine.SKILL_ITEM_STATE_ACTIVE:{
                this.selected();
                // deselected other skill
                if (this.onSelected) {
                    this.onSelected();
                }
                break;
            }
            case GameDefine.SKILL_ITEM_STATE_DEACTIVE:{
                break;
            }
            case GameDefine.SKILL_ITEM_STATE_SELECTED:{
                this.active();
                break;
            }
        }
    }

    public active(): void {
        this.setState(GameDefine.SKILL_ITEM_STATE_ACTIVE);
    }

    public deactive(): void {
        this.setState(GameDefine.SKILL_ITEM_STATE_DEACTIVE);
    }

    public selected(): void {
        this.setState(GameDefine.SKILL_ITEM_STATE_SELECTED);
    }

    public isSelected(): boolean {
        return this.state === GameDefine.SKILL_ITEM_STATE_SELECTED;
    }

    public decrease(): void {
        if (this.count < 0) {
            return;
        }
        this.count--;
        switch(this.state) {
            case GameDefine.SKILL_ITEM_STATE_ACTIVE:{
                if (this.count <= 0) {
                    this.deactive();
                }
                break;
            }
            case GameDefine.SKILL_ITEM_STATE_DEACTIVE:{
                break;
            }
            case GameDefine.SKILL_ITEM_STATE_SELECTED:{
                if (this.count > 0) {
                    this.active();
                } else {
                    this.deactive();
                }
                break;
            }
        }
    }

    public setTouchEnabled(touchEnabled: boolean): void {
        this.button.touchEnabled = touchEnabled
    }

}