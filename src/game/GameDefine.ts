class GameDefine {
    
    public static SHOW_DEBUG: boolean   = false;
    public static WIDTH: number         = 750;
    public static HEIGHT: number        = 1334;
    /*
    *   GUI
    */
    public static EXML_LOADING: string = "resource/game/gui/GUILoading.exml";
    public static EXML_WELCOME: string = "resource/game/gui/GUIWelcome.exml";
    public static EXML_INGAME:  string = "resource/game/gui/GUIInGame.exml";
    public static EXML_GAMEOVER:string = "resource/game/gui/GUIGameOver.exml";
    public static EXML_TUTORIAL:string = "resource/game/gui/GUITutorial.exml";

    /*
    * State
    */
    public static GAME_STATE = {
        STATE_NONE:     0,
        STATE_LOADING:  1,
        STATE_WELCOME:  2,
        STATE_INGAME:   3,
        STATE_GAMEOVER: 4
    }

    /*
    * BALL
    */
    public static ALL_BALL_COLORs: BallColor[] = [  BallColor.BLUE, 
                                                    BallColor.DEEP_BLUE, 
                                                    BallColor.GREEN, 
                                                    BallColor.LIGHT_RED, 
                                                    BallColor.RED, 
                                                    BallColor.VIOLET, 
                                                    BallColor.YELLOW];
    public static MATRIX_SIZE: number           = 9;
    public static MIN_SCORE: number             = 5;
    public static BOARD_OFFSET_X: number        = 0;
    public static ADD_NUM_BALLs: number         = 3;
    
    /*
    *   SKILL ITEM
    */
    public static SKILL_ITEM_STATE_ACTIVE   = 0;
    public static SKILL_ITEM_STATE_DEACTIVE = 1;
    public static SKILL_ITEM_STATE_SELECTED = 2;

    public static SKILL_BREAK_MAX           = 1;
    public static SKILL_FROZEN_MAX          = 1;
    public static SKILL_UNDO_MAX            = 1;
    
}