class Board extends egret.Sprite {
    
    public cellsBallSprite: BallSprite[][]  = [];
    public lineBoard: egret.Bitmap          = null;
    public cellW: number                    = 0;
    public cellH: number                    = 0;

    public constructor() {
        super();
        
        this.lineBoard = GameUtils.createBitmapByName("AP_grid_png");
        this.addChild(this.lineBoard);
    }

    public init(): void {

        if (!this.cellsBallSprite.length) {
            
            this.alignWidthBoardFullScreen();
            
            // init cell
            this.cellW = this.width / GameDefine.MATRIX_SIZE;
            this.cellH = this.height / GameDefine.MATRIX_SIZE;

            // create all balls
            let color = null;
            let ball  = null;
            for (let i = 0; i < GameDefine.MATRIX_SIZE; i++) {
                this.cellsBallSprite[i] = [];
                for (let j = 0; j < GameDefine.MATRIX_SIZE; j++) {
                    ball  = new BallSprite();
                    ball.setIndex(i, j);
                    this.addChild(ball);
                    this.alignBall(ball, i, j);
                    this.cellsBallSprite[i][j] = ball;
                }
            }

            this.addDebugRect();
        }

        this.hideAllBalls();
    }

    private alignWidthBoardFullScreen(): void {
        let ratio = GameControl.getInstance().getScaleRatio();
        this.scaleX = this.scaleY = Math.min(this.$stage.$stageWidth / ratio - GameDefine.BOARD_OFFSET_X * 2, GameDefine.WIDTH) / this.width;
        //GameControl.getInstance().alignCenterStage(this);

        let stage = GameControl.getInstance().getStage();
        this.x = (stage.stageWidth - this.width * this.scaleX) / 2;
        this.y = (stage.stageHeight - this.height * this.scaleY) / 2 + 80;
    }

    public alignBoard(): void {
        
    }

    public alignBall(ball: BallSprite, row: number, column: number): void {
        ball.x = this.cellW * (column + 0.5);
        ball.y = this.cellH * (row + 0.5);
    }

    public hideAllBalls(): void {
        for (let i = 0; i < GameDefine.MATRIX_SIZE; i++) {
            for (let j = 0; j < GameDefine.MATRIX_SIZE; j++) {
                this.cellsBallSprite[i][j].hide();
            }
        }
    }

    public showTargetBall(fromBall: BallSprite, toBall: BallSprite): void {
        let f = this.cellsBallSprite[fromBall.row][fromBall.column];
        let t = this.cellsBallSprite[toBall.row][toBall.column];

        t.init(f.color);
        t.setActive(true);

        f.hide();
        this.alignBall(f, f.row, f.column);
    }

    public getCellTouched(tx: number, ty: number): number[] {
        let row = Math.floor(ty / this.cellW);
        let column = Math.floor(tx / this.cellH);
        return [row, column];
    }

    public getMovePath(fromI: number, fromJ: number, toI: number, toJ: number): CellList {
        let dadi: number[][] = GameUtils.init2DimensionsArray(-1, GameDefine.MATRIX_SIZE);
        let dadj: number[][] = GameUtils.init2DimensionsArray(0, GameDefine.MATRIX_SIZE);;
        let queuei: number[] = GameUtils.initArray(0, GameDefine.MATRIX_SIZE);
        let queuej: number[] = GameUtils.initArray(0, GameDefine.MATRIX_SIZE);;

        let u: number[] = [1, 0, -1, 0];
        let v: number[] = [0, 1, 0, -1];
        let first = 0, last = 0, x = 0, y = 0, xx = 0, yy = 0, i = 0, j = 0, k = 0;
        let cellPath: CellList = new CellList();

        queuei[0] = toI;
        queuej[0] = toJ;
        dadi[toI][toJ] = -2;
        while (first <= last) {
            x = queuei[first];
            y = queuej[first];
            first++;
            for (k = 0; k < 4; k++) {
                xx = x + u[k];
                yy = y + v[k];
                if (xx == fromI && yy == fromJ) {
                    dadi[fromI][fromJ] = x;
                    dadj[fromI][fromJ] = y;
                    i = 0;
                    while (true) {
                        if (cellPath.list[i] == null) { 
                            cellPath.list[i] = []; 
                        }
                        cellPath.list[i][0] = fromI;
                        cellPath.list[i][1] = fromJ;
                        i++;
                        k = fromI;
                        fromI = dadi[fromI][fromJ];
                        if (fromI == -2) break;
                        fromJ = dadj[k][fromJ];
                    }
                }
                if (this.isInside(xx, yy))
                    if (dadi[xx][yy] == -1 && this.isValidToMoveOn(xx, yy)) {
                        last++;
                        queuei[last] = xx;
                        queuej[last] = yy;
                        dadi[xx][yy] = x;
                        dadj[xx][yy] = y;
                    }
            }
        }
        return cellPath;
    }

    private isInside(x: number, y: number): boolean {
        return x >= 0 && x < GameDefine.MATRIX_SIZE && y >= 0 && y < GameDefine.MATRIX_SIZE;
    }

    public removeBall(i: number, j: number): void {
        let ballSprite = this.cellsBallSprite[i][j];
        ArmatureUtil.runDestroy(ballSprite.armatureDisplay);
        setTimeout(function () {
            if (ballSprite != null) {
                ArmatureUtil.stopDestroyAnim(ballSprite.armatureDisplay);
                GameUtils.removeChild(ballSprite);
            }
        }, 200);
        this.setCellBallSprite(null, i, j);


    }

    public getScoredLine(iCenter: number, jCenter: number): CellList {
        let cellPath: CellList = new CellList();
        let u = [0, 1, 1, 1];
        let v = [1, 0, -1, 1];
        let i = 0, j = 0, k = 0, t = 0, count = 0;
        for (t = 0; t < 4; t++) {
            k = 0;
            i = iCenter;
            j = jCenter;
            while (true) {
                i += u[t];
                j += v[t];
                if (!this.isInside(i, j))
                    break;
                if (!this.cellsBallSprite[i][j].visible || !this.cellsBallSprite[iCenter][jCenter].visible)
                    break;
                if (this.cellsBallSprite[i][j].color != this.cellsBallSprite[iCenter][jCenter].color)
                    break;
                k++;
            }
            i = iCenter;
            j = jCenter;
            while (true) {
                i -= u[t];
                j -= v[t];
                if (!this.isInside(i, j))
                    break;
                if (!this.cellsBallSprite[i][j].visible || !this.cellsBallSprite[iCenter][jCenter].visible)
                    break;
                if (this.cellsBallSprite[i][j].color != this.cellsBallSprite[iCenter][jCenter].color)
                    break;
                k++;
            }
            k++;
            if (k >= GameDefine.MIN_SCORE) {
                for (let z = 0; z < k; z++) {
                    i += u[t];
                    j += v[t];
                    if (this.isInside(i, j) && (i != iCenter || j != jCenter)) {
                        if (cellPath.list[count] == null) cellPath.list[count] = [];
                        cellPath.list[count][0] = i;
                        cellPath.list[count][1] = j;
                        count++;
                    }
                }
            }
        }
        if (cellPath.list.length > 0) {
            if (cellPath.list[count] == null) cellPath.list[count] = [];
            cellPath.list[count][0] = iCenter;
            cellPath.list[count][1] = jCenter;
        }
        return cellPath;
    }

    public setCellBallSprite(ballSprite: BallSprite, i: number, j: number): void {
        this.cellsBallSprite[i][j] = ballSprite;
        // console.log("cell " + i + "," + j + " " + this.cellsHasBall[i][j]);
    }

    public getEmptyCells(): number[][] {
        let emptyCells: number[][] = [];
        let cnt = 0;
        for (let i = 0; i < GameDefine.MATRIX_SIZE; i++)
            for (let j = 0; j < GameDefine.MATRIX_SIZE; j++)
                if (!this.cellsBallSprite[i][j].visible) {
                    if (emptyCells[cnt] == null) { 
                        emptyCells[cnt] = [];
                    }
                    emptyCells[cnt] = [i, j];
                    cnt++;
                }
        return emptyCells;
    }

    public isCellEmpty(i: number, j: number): boolean {
        return !this.cellsBallSprite[i][j].visible;
    }

    public isValidToMoveOn(row: number, column: number): boolean {
        if (!this.isInside(row, column)) {
            return false;
        }
        let ball = this.getBallAt(row, column);
        if (ball.isActive) {
            ball.isActive
        }
        return !ball.visible || !ball.isActive;
    }

    public getBallAt(row: number, column: number): BallSprite {
        return this.isInside(row, column) ? this.cellsBallSprite[row][column] : null;
    }

    public getCellPos(row: number, column: number) {
        return {x: this.cellW * (column + 0.5),
                y: this.cellH * (row + 0.5)}
    }

    public hideBalls(cellList: CellList): void {
        for (let i = 0; i < cellList.list.length; i++) {
            this.cellsBallSprite[cellList.list[i][0]][cellList.list[i][1]].runAnimHide();
        }
    }

    public addDebugRect(): void {
        if (!DEBUG || !GameDefine.SHOW_DEBUG) {
            return;
        }
        
        let thickness = 2;
        let rect = new egret.Shape();
        rect.graphics.lineStyle(thickness, 0xff0000);
        rect.graphics.drawRect(-thickness, 
                                    -thickness, 
                                    this.width + thickness * 2, 
                                    this.height + thickness * 2);
        rect.graphics.endFill();      
        this.addChild(rect);
    }
}