class Player {

    imgAvatar: eui.Image;
    lbName: eui.Label;
    lbScore: eui.Label;

    score: number;

    constructor(avatar: eui.Image, name: eui.Label, score: eui.Label) {
        this.imgAvatar = avatar;
        this.lbName    = name;
        this.lbScore   = score; 
    }

    public init(): void {
        this.score = 0;
        this.addScore(0);
    }

    public addScore(score: number) {
        this.score += score;
        this.lbScore.text = this.score + "";
    }

}