class GameManager extends egret.DisplayObjectContainer {

    public static gameManager: GameManager;

    private lineBoard: Board;
    private stageW: number;
    private stageH: number;
    private cellW: number;
    private cellH: number;

    private currentBall: BallSprite;
    private scoredText: egret.TextField;
    private commingBalls: BallSprite[] = [];
    private previousCommingBalls: any = [];
    private nextBall: NextBall;

    private targetBall: BallSprite = null;
    private moveBall: BallSprite = null;
    private canUndo: boolean = false;
    public skillFrozenPrepare: boolean = false;
    private trackBall = { from: null, to: null };
    private step: number = 0;
    private stepBroken: number = 0;
    private brokenBall: BallSprite;

    private constructor() {
        super();
    }

    public static getInstance(): GameManager {
        if (!this.gameManager) {
            this.gameManager = new GameManager();
        }
        return this.gameManager;
    }

    public init(): void {
        if (!this.lineBoard) {
            this.lineBoard = new Board();
            this.addChild(this.lineBoard);

            this.lineBoard.lineBoard.touchEnabled = true;
            this.lineBoard.lineBoard.addEventListener(egret.TouchEvent.TOUCH_END, this.boardTouchHandler, this);

            this.nextBall = new NextBall();
            this.addChild(this.nextBall);

        }
        this.lineBoard.init();
        this.addBallsToBoard(GameDefine.ADD_NUM_BALLs, true);
        this.addBallsToBoard(GameDefine.ADD_NUM_BALLs, false);
        this.canUndo = false;
        this.step = 1;
        this.stepBroken = -1;
        this.brokenBall = null;
    }

    public addBallsToBoard(numBalls: number, active: boolean): void {

        //check avail cells
        let emptyCells = this.lineBoard.getEmptyCells();
        if (!emptyCells.length) {
            StateManager.getInstance().switchToState(GameDefine.GAME_STATE.STATE_GAMEOVER);
            return;
        }

        numBalls = Math.min(emptyCells.length, numBalls);
        let usedIndex: number[] = GameUtils.initArray(0, emptyCells.length);

        for (let i = 0; i < numBalls; i++) {
            let index = GameUtils.getRandomNumber(0, emptyCells.length - 1);
            while (usedIndex[index] == 1) {
                index = GameUtils.getRandomNumber(0, emptyCells.length - 1);
            }
            usedIndex[index] = 1;
            let rColor = GameUtils.getRandomNumber(0, GameDefine.ALL_BALL_COLORs.length - 1);
            let bSprite = this.lineBoard.getBallAt(emptyCells[index][0], emptyCells[index][1]);

            if (bSprite) {
                if (i < this.nextBall.balls.length) {
                    bSprite.init(this.nextBall.balls[i].color);
                } else {
                    bSprite.init(GameDefine.ALL_BALL_COLORs[rColor]);
                }

                bSprite.setIndex(emptyCells[index][0], emptyCells[index][1]);
                bSprite.setActive(active);

                if (!active) {
                    this.commingBalls[i] = bSprite;
                }

                if (!bSprite.hasEventListener(egret.TouchEvent.TOUCH_END)) {
                    bSprite.addEventListener(egret.TouchEvent.TOUCH_END, this.ballTouchHandler, this);
                }
            } else {
                console.log(`can not get ball from board at row=${emptyCells[index][0]} column=${emptyCells[index][1]}`);
            }

        }

        this.nextBall.random();
    }

    // a deactive ball will be display at the next step if it was override
    private changeABallToCommingBalls(color: BallColor): void {
        let emptyCells = this.lineBoard.getEmptyCells();
        let ball: BallSprite = this.moveBall;
        let getBallFromRandom: boolean = false;
        ball.init(color);
        ball.setActive(false);

        if (emptyCells.length) {
            let index = GameUtils.getRandomNumber(0, emptyCells.length - 1);
            let bSprite = this.lineBoard.getBallAt(emptyCells[index][0], emptyCells[index][1]);

            if (bSprite && (emptyCells.length != 1 || bSprite.row != ball.row || bSprite.column != ball.column)) {
                ball.hide();
                bSprite.init(color);
                bSprite.setIndex(emptyCells[index][0], emptyCells[index][1]);
                bSprite.setActive(false);
                //bSprite.hide();
                ball = bSprite;
                if (!bSprite.hasEventListener(egret.TouchEvent.TOUCH_END)) {
                    bSprite.addEventListener(egret.TouchEvent.TOUCH_END, this.ballTouchHandler, this);
                }
                getBallFromRandom = true;
            }
        }

        for (let i = 0; i < this.commingBalls.length; ++i) {
            if (this.commingBalls[i] &&
                this.commingBalls[i].row == this.targetBall.row &&
                this.commingBalls[i].column == this.targetBall.column) {
                this.commingBalls[i] = ball;
                this.trackBall.from = {};
                this.trackBall.from.row = this.targetBall.row;
                this.trackBall.from.column = this.targetBall.column;
                this.trackBall.to = {};
                this.trackBall.to.row = ball.row;
                this.trackBall.to.column = ball.column;
                break;
            }
        }
    }

    private boardTouchHandler(event: egret.TouchEvent): void {
        let touchedCell = this.lineBoard.getCellTouched(event.localX, event.localY);
        if (this.currentBall != null) {
            if (this.currentBall.row == touchedCell[0] && this.currentBall.column == touchedCell[1]) return;
            if (this.lineBoard.isValidToMoveOn(touchedCell[0], touchedCell[1])) {
                let cellPath = this.lineBoard.getMovePath(this.currentBall.row, this.currentBall.column, touchedCell[0], touchedCell[1]);
                if (cellPath.list.length > 0) {
                    this.changeBallPos(this.currentBall, cellPath);
                } else {//else remove the scored ball;
                    console.log("can't move ball " + this.currentBall.row + "," + this.currentBall.column + " to " + touchedCell[0] + "," + touchedCell[1]);
                }
            }
        }
    }

    private ballTouchHandler(event: egret.TouchEvent): void {
        let bs = <BallSprite>event.currentTarget;

        // check skill break is selected;
        if (SkillManager.getInstance().isSelectedBreak()) {
            bs.runAnimHide();
            SkillManager.getInstance().decreaseBreak();
            this.increaseStep();
            //if (this.step == 2) {
                this.canUndo = true;
                this.brokenBall = bs;
                this.stepBroken = this.step - 1;
            //}
            return;
        }

        // normal action
        if (this.currentBall != null) {
            ArmatureUtil.stopActiveAnim(this.currentBall.armatureDisplay);
        }
        this.currentBall = bs;
        ArmatureUtil.runActive(this.currentBall.armatureDisplay);

        this.skillFrozenPrepare = true;
    }

    private changeBallPos(bSprite: BallSprite, cellPath: CellList) {
        let destinationCell = cellPath.list.length - 1;
        this.targetBall = this.lineBoard.getBallAt(cellPath.list[destinationCell][0], cellPath.list[destinationCell][1]);
        if (!this.lineBoard.isValidToMoveOn(this.targetBall.row, this.targetBall.column)) {
            return;
        }
        this.moveBall = bSprite;
        console.log("move ball from " + bSprite.row + "," + bSprite.column + " to " + this.targetBall.row + "," + this.targetBall.column);
        this.animMoveBall(bSprite, cellPath);
        this.currentBall = null;
    }

    private animMoveBall(bSprite: BallSprite, cellPath: CellList): void {
        this.lineBoard.setChildIndex(bSprite, this.lineBoard.$children.length - 1);
        ArmatureUtil.stopActiveAnim(bSprite.armatureDisplay);
        // console.log(bSprite.x + "," + bSprite.y);
        let mov = egret.Tween.get(bSprite, { loop: false });
        for (let i = 1; i < cellPath.list.length; i++) {
            let cell = this.lineBoard.getCellPos(cellPath.list[i][0], cellPath.list[i][1]);
            mov.to({ x: cell.x, y: cell.y }, 50);

        }
        mov.call(this.moveBallComplete, this)
    }

    private moveBallComplete(): void {
        if (!this.moveBall || !this.targetBall) {
            return;
        }
        if (!this.targetBall.hasEventListener(egret.TouchEvent.TOUCH_END)) {
            this.targetBall.addEventListener(egret.TouchEvent.TOUCH_END, this.ballTouchHandler, this);
        }
        let isTargetVisible = this.targetBall.visible; // visible but deactive ~ small ball
        let targetColor = this.targetBall.color;
        this.lineBoard.showTargetBall(this.moveBall, this.targetBall);

        // back up ball that was orverride
        if (isTargetVisible) {
            this.changeABallToCommingBalls(targetColor);
        }

        let score = this.checkScored(this.targetBall.row, this.targetBall.column);
        PlayerManager.getInstance().addScore(score);
        if (!SkillManager.getInstance().isSelectedFrozen()) {
            if (!score) {
                this.activeCommingBalls();
                this.addBallsToBoard(GameDefine.ADD_NUM_BALLs, false);
            }
        } else {
            SkillManager.getInstance().decreaseFrozen();
            SkillManager.getInstance().hideBox();
            this.skillFrozenPrepare = false;
        }
        this.canUndo = true;
        this.increaseStep();
    }

    private checkScored(row: number, column: number): number {
        let scoreLine = this.lineBoard.getScoredLine(row, column);
        if (scoreLine.list.length > 0) {
            //add score
            console.log("add score " + scoreLine.list.length);
            this.lineBoard.hideBalls(scoreLine);
        }
        return scoreLine.list.length;
    }

    private activeCommingBalls(): void {
        this.previousCommingBalls = [];
        for (let i = 0; i < this.commingBalls.length; i++) {
            let bSprite = this.commingBalls[i];
            if (bSprite == null ||
                this.lineBoard.isCellEmpty(bSprite.row, bSprite.column)) {
                this.previousCommingBalls[i] = null;
                continue;
            }
            // backup for undo
            this.previousCommingBalls[i] = {};
            if (this.trackBall.from &&
                this.trackBall.to &&
                this.trackBall.to.row == bSprite.row &&
                this.trackBall.to.column == bSprite.column) {
                this.previousCommingBalls[i].row = this.trackBall.from.row;
                this.previousCommingBalls[i].column = this.trackBall.from.column;
                this.previousCommingBalls[i].color = bSprite.color;
            } else {
                this.previousCommingBalls[i].row = bSprite.row;
                this.previousCommingBalls[i].column = bSprite.column;
                this.previousCommingBalls[i].color = bSprite.color;
            }


            // active next ball
            bSprite.init(bSprite.color);
            bSprite.setActive(true);
            PlayerManager.getInstance().addScore(this.checkScored(bSprite.row, bSprite.column));
            this.commingBalls[i] = null;
        }

    }

    public isCanUndo(): boolean {
        return this.canUndo;
    }

    public undo(): void {
        this.canUndo = false;
        if (this.brokenBall && this.step == this.stepBroken + 1) {
            this.brokenBall.init(this.brokenBall.color);
            this.brokenBall.setActive(true);
            return;
        }

        // undo main ball
        this.moveBall.init(this.targetBall.color);
        this.moveBall.setActive(true);
        this.targetBall.hide();

        if (!this.previousCommingBalls.length) {
            return;
        }

        // undo commingBalls
        let ball: BallSprite;
        for (let i = 0; i < this.commingBalls.length; ++i) {
            if (!this.commingBalls[i]) {
                continue;
            }
            ball = this.commingBalls[i];
            ball.hide();
        }
        this.commingBalls = []
        let info: any = {};
        for (let i = 0; i < this.previousCommingBalls.length; ++i) {
            if (!this.previousCommingBalls[i]) {
                this.commingBalls[i] = null;
                continue;
            }
            info = this.previousCommingBalls[i];
            ball = this.lineBoard.getBallAt(info.row, info.column);
            ball.init(info.color);
            ball.setActive(false);
            this.commingBalls[i] = ball;
        }
        if (this.trackBall.to) {
            let ball = this.lineBoard.getBallAt(this.trackBall.to.row, this.trackBall.to.column);
            if (ball) {
                ball.hide();
            }
        }
        this.trackBall = { from: null, to: null };
        this.increaseStep();
    }

    private increaseStep(): void {
        this.step++;
    }
}
