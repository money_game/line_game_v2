class GameControl {

    public static _gameControl: GameControl = null;

    private stage: egret.Stage = null;
    
    private constructor() {
    }
    
    public static getInstance(): GameControl {
        if (!this._gameControl) {
            this._gameControl = new GameControl();
        }
        return this._gameControl;
    }

    public setStageHandler(stage: egret.Stage): void {
        this.stage = stage;
    }

    public getStage(): egret.Stage {
        return this.stage;
    }

    /*
    *   Offset use to align eui in GUI
    */
    public getGuiOffsetX(): number {
        //return this.stage.stageHeight * GameDefine.WIDTH / GameDefine.HEIGHT - this.stage.stageWidth;
        let screen = this.getScreenSize(GameManager.getInstance());
        return (GameDefine.WIDTH - screen.width)/2;
    }

    /*
    *   Get offset of screen
    */
    public getScreenOffset(state: egret.DisplayObjectContainer) {
        let global = state.globalToLocal(0, 0);
        return {x: global.x, 
                y: global.y};
    }

    /*
    *   Get full screen size
    */
    public getScreenSize(state: egret.DisplayObjectContainer) {
        let offset = this.getScreenOffset(state);
        return {x: offset.x,
                y: offset.y,
                width: this.stage.stageWidth - offset.x * 2,
                height: this.stage.stageHeight - offset.y * 2}
    }

    /*
    *
    */
    public getScaleRatio(): number {
        return this.stage.stageHeight / GameDefine.HEIGHT;
    }

    /*
    *   Align state center screen
    */
    public alignState(state: egret.DisplayObjectContainer): void {
        state.scaleX = state.scaleY = this.getScaleRatio();
        state.anchorOffsetX = this.stage.stageWidth / 2;
        state.anchorOffsetY = this.stage.stageHeight / 2;
        state.x = this.stage.stageWidth / 2;
        state.y = this.stage.stageHeight / 2;
    }

    /*
    *   Align object center screen
    */
    public alignCenterStage(object: egret.DisplayObject): void {
        object.x = (this.stage.stageWidth - object.width * object.scaleX) / 2;
        object.y = (this.stage.stageHeight - object.height * object.scaleY) / 2;
    }

    /*
    *   Align All UI In GUI
    */
    public alignAllUIInGUI(gui: egret.DisplayObjectContainer): void {
        
        let i = 0;
        let e = null;
        let guiOX = this.getGuiOffsetX();

        for(; i < gui.$children.length; ++i) {
            e = gui.$children[i];
            if (isNaN(e.horizontalCenter)) {
                if (isNaN(e.left)) {
                    if (isNaN(e.right)) { // align x

                    } else { // align right
                        if (!e.rightOrginal) {
                            e.rightOrginal = e.right;
                        }
                        e.right = e.rightOrginal + guiOX;
                    }
                } else { // align left
                    if (!e.leftOrginal) {
                        e.leftOrginal = e.left;
                    }
                    e.left = e.leftOrginal + guiOX;
                }
            }
        }
    }

    /*
    *  Add rect to debug
    */
    public addDebugRect(state: egret.DisplayObjectContainer): void {
        if (!DEBUG || !GameDefine.SHOW_DEBUG) {
            return;
        }

        let thickness = 2;
        let insideRect = new egret.Shape();
        insideRect.graphics.lineStyle(thickness, 0xff0000);
        insideRect.graphics.drawRect(thickness, 
                                    thickness, 
                                    this.stage.stageWidth - thickness * 2, 
                                    this.stage.stageHeight - thickness * 2);
        insideRect.graphics.endFill();      
        state.addChild(insideRect);  

        let screen = this.getScreenSize(state);
        let outsideRect = new egret.Shape();
        outsideRect.graphics.lineStyle(thickness, 0x00ff00);
        outsideRect.graphics.drawRect(screen.x + thickness, 
                                    screen.y + thickness, 
                                    screen.width - thickness * 2, 
                                    screen.height - thickness * 2);
        outsideRect.graphics.endFill();      
        state.addChild(outsideRect);  

        let offsetX = this.getGuiOffsetX();
        let guiRect = new egret.Shape();
        let w = GameDefine.WIDTH - thickness * 2;
        guiRect.graphics.lineStyle(thickness, 0x0000ff);
        guiRect.graphics.drawRect( this.stage.stageWidth / 2 - w / 2, 
                                   screen.y + thickness, 
                                   w, 
                                   screen.height - thickness * 2);
        guiRect.graphics.endFill();      
        state.addChild(guiRect);  
    }
}