class NextBall extends egret.DisplayObjectContainer {
    
    public balls: BallSprite[] = [];

    constructor() {
        super();

        let color;
        let ball: BallSprite;
        for(let i = 0; i < 3; ++i) {
            color = GameUtils.getRandomNumber(0, GameDefine.ALL_BALL_COLORs.length - 1);
            ball = new BallSprite();
            ball.x = i * 80 + 20;       
            ball.init(GameDefine.ALL_BALL_COLORs[color]);     
            this.balls[i] = ball;
            this.addChild(ball);
        }

        this.addEventListener(eui.UIEvent.ADDED_TO_STAGE, this.onAdded, this);
    }

    private onAdded(): void {
        let stage = GameControl.getInstance().getStage();
        let screen = this.globalToLocal(0, 0);
        this.y = screen.y + 265;
        this.x = (this.stage.stageWidth - this.width) / 2;
    }

    public init(): void {

    }

    random() {
        let color;
        let ball: BallSprite;
        for (let i = 0; i < this.balls.length; ++i) {
            color = GameUtils.getRandomNumber(0, GameDefine.ALL_BALL_COLORs.length - 1);
            ball  = this.balls[i];
            ball.init(GameDefine.ALL_BALL_COLORs[color]);
            ball.setActive(true);
        }
    }
}