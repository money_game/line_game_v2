enum BallColor {
    BLUE = "ball_blue",
    DEEP_BLUE = "ball_gray",
    YELLOW = "ball_green",
    GREEN = "ball_orange",
    LIGHT_RED = "ball_red",
    RED = "ball_violet",
    VIOLET = "ball_yellow",
    NULL = "NULL"
}