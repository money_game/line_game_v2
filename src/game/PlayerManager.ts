class PlayerManager {

    static playerManager: PlayerManager;
    playerLeft: Player;
    playerRight: Player;
    isTurnLeft: boolean;

    public static getInstance(): PlayerManager {
        if (!this.playerManager) {
            this.playerManager = new PlayerManager();
        }
        return this.playerManager;
    }

    private constructor() {
    }

    public init(gui: eui.Component): void {
        if(!this.playerLeft) {
            let ingame: GUIInGame = <GUIInGame>gui;
            this.playerLeft = new Player(ingame.imgAvatarLeft, ingame.lbNameLeft, ingame.lbScoreLeft);
            this.playerRight = new Player(ingame.imgAvatarRight, ingame.lbNameRight, ingame.lbScoreRight);
        }
        this.playerLeft.init();
        this.playerRight.init();
        this.isTurnLeft = true;
    }

    public addScore(score: number): void {
        if (this.isTurnLeft) {
            this.playerLeft.addScore(score);
        } else {
            this.playerRight.addScore(score);
        }
    }

    public changeTurn(): void {
        this.isTurnLeft = !this.isTurnLeft;
    }

}