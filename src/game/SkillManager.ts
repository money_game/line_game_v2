class SkillManager extends egret.Sprite{
    
    public static skillManager: SkillManager;
    public skillBreak: SkillItem;
    public skillFrozen: SkillItem;

    private skillUndo: eui.Button;
    private countUndo: number;
    public box: egret.Sprite;
    private guide: egret.TextField;

    private constructor() {
        super();
    }

    public static getInstance(): SkillManager {
        if (!this.skillManager) {
            this.skillManager = new SkillManager();
        }
        return this.skillManager;
    }

    public init(gui: eui.Component): void {
        if (!this.skillBreak) {
            let guiInGame:GUIInGame = <GUIInGame>gui;
            this.skillBreak = new SkillItem(guiInGame.btnSkill1, GameDefine.SKILL_BREAK_MAX, this.onSelectedBreak);
            this.skillFrozen = new SkillItem(guiInGame.btnSkill2, GameDefine.SKILL_FROZEN_MAX, this.onSelectedFrozen);
            
            this.skillUndo = guiInGame.btnSkill3;
            if (this.skillUndo.hasEventListener(egret.TouchEvent.TOUCH_END)) {
                this.skillUndo.removeEventListener(egret.TouchEvent.TOUCH_END, this.onSelectedUndo, this);
            }
            this.skillUndo.addEventListener(egret.TouchEvent.TOUCH_END, this.onSelectedUndo, this);
            this.countUndo = GameDefine.SKILL_UNDO_MAX;

            let screenSize = GameControl.getInstance().getScreenSize(this);
            let height = 200;
            this.box = new egret.Sprite;
            this.box.graphics.beginFill( 0x000000, 0.7);
            this.box.graphics.drawRect(screenSize.x, screenSize.y + screenSize.height - height, screenSize.width, height);
            this.box.graphics.endFill();
            this.addChild(this.box);

            let stage = GameControl.getInstance().getStage();
            this.guide = new egret.TextField;
            this.guide.text = "";
            this.guide.textColor = 0xffffff;
            this.guide.size = 50;
            this.guide.bold = true;
            this.guide.textAlign = egret.HorizontalAlign.CENTER;
            this.guide.verticalAlign = egret.VerticalAlign.MIDDLE;
            this.guide.x = stage.stageWidth / 2 - this.guide.width / 2;
            this.guide.y = screenSize.y + screenSize.height - height / 2 - this.guide.size / 2;
            this.box.addChild(this.guide);
            this.box.visible = false;
        }
        this.updateUndoState();
    }

    private updateUndoState(): void {
        if (this.countUndo > 0) {
            this.skillUndo.enabled = true;
            this.skillUndo.touchEnabled = true;
        } else {
            this.skillUndo.enabled = false;
        }
    }

    private onSelectedBreak(): void {
        let instance = SkillManager.getInstance();
        instance.skillFrozen.updateState();

        let stage = GameControl.getInstance().getStage();
        instance.box.visible = true;
        instance.guide.text = "Chạm vào bóng để phá huỷ";
        instance.guide.x = stage.stageWidth / 2 - instance.guide.width / 2;

        instance.skillBreak.setTouchEnabled(false);
        instance.skillFrozen.setTouchEnabled(false);
        instance.skillUndo.touchEnabled = false;
    }

    private onSelectedFrozen(): void {
        let instance = SkillManager.getInstance();
        instance.skillBreak.updateState();

        let stage = GameControl.getInstance().getStage();
        instance.box.visible = true;
        instance.guide.text = "Đóng băng một lượt đi";
        instance.guide.x = stage.stageWidth / 2 - instance.guide.width / 2;

        instance.skillBreak.setTouchEnabled(false);
        instance.skillFrozen.setTouchEnabled(false);
        instance.skillUndo.touchEnabled = false;
    }

    private onSelectedUndo(): void {
        if (this.countUndo < 0 || !GameManager.getInstance().isCanUndo()) {
            return;
        }
        this.countUndo--;
        this.updateUndoState();
        this.skillBreak.updateState();
        this.skillBreak.updateState();
        // callback undo to gameplay
        GameManager.getInstance().undo();
       
    }

    public isSelectedBreak(): boolean {
        return this.skillBreak.isSelected();
    }

    public isSelectedFrozen(): boolean {
        return this.skillFrozen.isSelected();
    }

    public decreaseBreak(): void {
        this.skillBreak.decrease();
        this.skillFrozen.updateState();
        this.updateUndoState();
    }

    public decreaseFrozen(): void {
        this.skillFrozen.decrease();
        this.skillBreak.updateState();
        this.updateUndoState();
    }

    public hideBox(): void {
        if (this.box.visible) {
            this.box.visible = false;
        }
    }

    public onTouch(x: number, y: number) {
        if (!this.box.visible) {
            return;
        }
        let scale = GameControl.getInstance().getScaleRatio();
        if (y < this.skillBreak.button.y * scale) {
            if (this.skillFrozen.isSelected()) {
                let game = GameManager.getInstance();
                if (!game.skillFrozenPrepare) {
                    this.hideBox();
                    this.skillBreak.updateState();
                    this.skillFrozen.updateState();
                }
            } else {
                this.hideBox();
                this.skillBreak.updateState();
                this.skillFrozen.updateState();
            }
        }
    }
}