class StateManager extends egret.DisplayObjectContainer {

    public static _stateManager: StateManager            = null;

    private stateCurrent: egret.DisplayObjectContainer   = null;
    private stateLoading: StateLoading                   = null;
    private stateWelcome: StateWelcome                   = null;
    private stateInGame: StateInGame                     = null;
    private stateGameOver: StateGameOver                 = null;

    private constructor() {
        super();
    }

    public static getInstance(): StateManager {
        if (!this._stateManager) {
           this._stateManager = new StateManager(); 
        }
        return this._stateManager;
    }

    public switchToState(state:number):void {

        switch (state) {
            case GameDefine.GAME_STATE.STATE_NONE: {
                break;
            }
            case GameDefine.GAME_STATE.STATE_LOADING: {
                GameUtils.removeChild(this.stateCurrent);

                if (!this.stateLoading) {
                    this.stateLoading = new StateLoading();
                }
                this.stateCurrent = this.stateLoading;
                break;
            }
            case GameDefine.GAME_STATE.STATE_WELCOME: {
                GameUtils.removeChild(this.stateCurrent);

                if (!this.stateWelcome) {
                    this.stateWelcome = new StateWelcome();
                }
                this.stateCurrent = this.stateWelcome;
                break;
            }
            case GameDefine.GAME_STATE.STATE_INGAME: {
                GameUtils.removeChild(this.stateCurrent);
                
                if (!this.stateInGame) {
                    this.stateInGame = new StateInGame();
                }
                if (!this.stateGameOver) {
                    this.stateGameOver = new StateGameOver();
                }

                this.stateInGame.load();
                this.stateCurrent = this.stateInGame;
                break;
            }
            case GameDefine.GAME_STATE.STATE_GAMEOVER: {
                this.stateCurrent = this.stateGameOver;
                break;
            }
        }

        this.addChild(this.stateCurrent);
    }
}