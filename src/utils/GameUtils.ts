class GameUtils {
    
    public static createBitmapByName(name: string): egret.Bitmap {
        let result = new egret.Bitmap();
        let texture: egret.Texture = RES.getRes(name);
        result.texture = texture;
        return result;
    }
    
    public static removeChild (child:egret.DisplayObject) {
        if (child && child.parent) {
            child.parent.removeChild(child);
        }
    }

    public static getRandomNumber(min: number, max: number): number {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    public static init2DimensionsArray(fill: number, size: number): number[][] {
        let res: number[][] = [];
        for (let x = 0; x < size; x++)
            for (let y = 0; y < size; y++) {
                if (res[x] == null) res[x] = [];
                res[x][y] = fill;
            }
        return res;
    }

    public static initArray(fill: number, size: number): number[] {
        let res: number[] = [];
        for (let x = 0; x < size; x++)
            res[x] = fill;
        return res;
    }

    public static isTouchOn(btn: any, touchX: number, touchY: number) {
        let scale = GameControl.getInstance().getScaleRatio();
        let g = btn.globalToLocal(0, 0);
        
        let tx = touchX;
        let ty = touchY;
        if (tx < btn.x - g.x ||
            tx > btn.x - g.x + btn.width * scale||
            ty < btn.y * scale ||
            ty > btn.y * scale + btn.height * scale) {
                return false;
            }
        return true;
    }
}