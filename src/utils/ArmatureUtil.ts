class ArmatureUtil {
    private static _instance: ArmatureUtil = null;
    private static activeAnim: string = "choose";
    private static destroyAnim: string = "destroy";
    private factory: dragonBones.EgretFactory;
    public static getInstance(): ArmatureUtil {
        if (!ArmatureUtil._instance) {
            ArmatureUtil._instance = new ArmatureUtil();
        }
        return ArmatureUtil._instance;
    }
    private constructor() {
        this.factory = new dragonBones.EgretFactory();
        let skeletonData = RES.getRes("ball_line_ske_json");
        let textureData = RES.getRes("ball_line_tex_json");
        let texture = RES.getRes("ball_line_tex_png");
        this.factory.parseDragonBonesData(skeletonData);
        this.factory.parseTextureAtlasData(textureData, texture);
    }
    public getArmatureDisplay(armatureName: string): dragonBones.EgretArmatureDisplay {
        return this.factory.buildArmatureDisplay(armatureName, "ball_line");
    }
    public getArmature(armatureName: string): dragonBones.Armature {
        return this.factory.buildArmature(armatureName, "ball_line");
    }
    public static runActive(armature: dragonBones.EgretArmatureDisplay):void {
        armature.animation.play(ArmatureUtil.activeAnim);
    }
    public static runDestroy(armature: dragonBones.EgretArmatureDisplay):void {
        armature.animation.play(ArmatureUtil.destroyAnim, 1);
    }
    public static stopAnim(armature: dragonBones.EgretArmatureDisplay):void {
        armature.animation.stop();
    }
    public static stopActiveAnim(armature: dragonBones.EgretArmatureDisplay):void {
        armature.animation.stop(ArmatureUtil.activeAnim);
    }
    public static stopDestroyAnim(armature: dragonBones.EgretArmatureDisplay):void {
        armature.animation.stop(ArmatureUtil.destroyAnim);
    }
}