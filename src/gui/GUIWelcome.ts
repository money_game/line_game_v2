class GUIWelcome extends eui.Component implements  eui.UIComponent {

	public btnPlay: eui.Button;
    public callbackBtnPlay: Function;

	public constructor() {
		super();
		this.addEventListener(eui.UIEvent.COMPLETE, this.init, this);
        this.skinName = GameDefine.EXML_WELCOME;
	}

	protected partAdded(partName:string, instance:any): void {
		super.partAdded(partName, instance);
	}


	protected childrenCreated(): void {
		super.childrenCreated();
	}

    public setCallbackBtnPlay(cb: Function): void {
        this.callbackBtnPlay =  cb;
    }

	private init(): void {
		let gameControl = GameControl.getInstance();
		gameControl.alignCenterStage(this);
		gameControl.alignAllUIInGUI(this);

        this.btnPlay.addEventListener(egret.TouchEvent.TOUCH_END, this.onTouchBtnPlay, this)
	}

    private onTouchBtnPlay(event: egret.TouchEvent): void {
        if (this.callbackBtnPlay) {
            this.callbackBtnPlay();
            this.btnPlay.$setTouchEnabled(false);
		}
	}
}