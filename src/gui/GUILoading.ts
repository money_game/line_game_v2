class GUILoading extends eui.Component implements  eui.UIComponent {

	public loadingText: eui.Label;

	public constructor() {
		super();
		this.addEventListener(eui.UIEvent.COMPLETE, this.init, this);
        this.skinName = GameDefine.EXML_LOADING;
	}

	protected partAdded(partName:string, instance:any): void {
		super.partAdded(partName, instance);
	}


	protected childrenCreated(): void {
		super.childrenCreated();
	}

	private init(): void {
		let gameControl = GameControl.getInstance();
		gameControl.alignCenterStage(this);
		gameControl.alignAllUIInGUI(this);
	}
	
	public setProgress(current:number, total:number):void {
        this.loadingText.text = "Loading...";//`Loading...${current}/${total}`;
    }
}