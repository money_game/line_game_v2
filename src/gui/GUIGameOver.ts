class GUIGameOver extends eui.Component implements  eui.UIComponent {

	public btnReplay: eui.Button;
	public btnMainMenu: eui.Button;
	public lbScore: eui.Label;
    public callbackBtnReplay: Function;
	public callbackBtnMainMenu: Function;

	public constructor() {
		super();
		this.addEventListener(eui.UIEvent.COMPLETE, this.init, this);
        this.skinName = GameDefine.EXML_GAMEOVER;
	}

	protected partAdded(partName:string, instance:any): void {
		super.partAdded(partName, instance);
	}


	protected childrenCreated(): void {
		super.childrenCreated();
	}

    public setCallbackBtnReplay(cb: Function): void {
        this.callbackBtnReplay = cb;
    }

	public setCallbackBtnMainMenu(cb: Function): void {
        this.callbackBtnMainMenu = cb;
    }

	private init(): void {
		let gameControl = GameControl.getInstance();
		gameControl.alignCenterStage(this);
		gameControl.alignAllUIInGUI(this);

        this.btnReplay.addEventListener(egret.TouchEvent.TOUCH_END, this.onTouchBtnReplay, this);
		this.btnMainMenu.addEventListener(egret.TouchEvent.TOUCH_END, this.onTouchbtnMainMenu, this);
	}

    private onTouchBtnReplay(event: egret.TouchEvent): void {
        if (this.callbackBtnReplay) {
            this.callbackBtnReplay();
            this.btnReplay.$setTouchEnabled(false);
        }
    }

	private onTouchbtnMainMenu(event: egret.TouchEvent): void {
		if (this.callbackBtnMainMenu) {
            this.callbackBtnMainMenu();
            this.btnMainMenu.$setTouchEnabled(false);
        }
	}
}