class GUIInGame extends eui.Component implements  eui.UIComponent {

	public imgAvatarLeft: eui.Image;
	public lbNameLeft: eui.Label;
	public lbScoreLeft: eui.Label;

	public imgAvatarRight: eui.Image;
	public lbNameRight: eui.Label;
	public lbScoreRight: eui.Label;

	public btnSkill1: eui.Button;
	public btnSkill2: eui.Button;
	public btnSkill3: eui.Button;

	public constructor() {
		super();
		this.addEventListener(eui.UIEvent.COMPLETE, this.init, this);
        this.skinName = GameDefine.EXML_INGAME;
		this.currentState = "ingame";
	}

	protected partAdded(partName:string, instance:any): void {
		super.partAdded(partName, instance);
	}


	protected childrenCreated(): void {
		super.childrenCreated();
	}

	private init(): void {
		let gameControl = GameControl.getInstance();
		gameControl.alignCenterStage(this);
		gameControl.alignAllUIInGUI(this);
	}
}