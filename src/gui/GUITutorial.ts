class GUITutorial extends eui.Component implements  eui.UIComponent {

	public constructor() {
		super();
        this.skinName = GameDefine.EXML_TUTORIAL;
        this.addEventListener(eui.UIEvent.COMPLETE, this.init, this);
	}

	protected partAdded(partName:string, instance:any): void {
		super.partAdded(partName, instance);
	}


	protected childrenCreated(): void {
		super.childrenCreated();
	}

    private init(): void {
		let gameControl = GameControl.getInstance();
		gameControl.alignCenterStage(this);
		gameControl.alignAllUIInGUI(this);
	}
}