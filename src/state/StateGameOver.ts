class StateGameOver extends egret.DisplayObjectContainer {

    private guiGameOver: GUIGameOver;

    public constructor() {
		super();

		this.guiGameOver = new GUIGameOver();
		this.addChild(this.guiGameOver);
		
		this.addEventListener(eui.UIEvent.ADDED_TO_STAGE, this.init, this);
	}

    private init(): void {
		let gameControl = GameControl.getInstance();
		gameControl.alignState(this);
		gameControl.addDebugRect(this);
        
        this.guiGameOver.setCallbackBtnReplay(this.onTouchBtnReplay);
        if (this.guiGameOver.btnReplay) {
			this.guiGameOver.btnReplay.$setTouchEnabled(true);
		}

		this.guiGameOver.setCallbackBtnMainMenu(this.onTouchBtnMainMenu);
		if (this.guiGameOver.btnMainMenu) {
			this.guiGameOver.btnMainMenu.$setTouchEnabled(true);
		}

		this.guiGameOver.lbScore.text = PlayerManager.getInstance().playerLeft.score + "";
	}

    public onTouchBtnReplay(event: egret.TouchEvent): void {
        StateManager.getInstance().switchToState(GameDefine.GAME_STATE.STATE_INGAME);
    }

	public onTouchBtnMainMenu(event: egret.TouchEvent): void {
		StateManager.getInstance().switchToState(GameDefine.GAME_STATE.STATE_WELCOME);
	}
}