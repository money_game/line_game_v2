class StateWelcome extends egret.DisplayObjectContainer {

    private guiWelcome: GUIWelcome;

    public constructor() {
		super();

		this.guiWelcome = new GUIWelcome();
		this.addChild(this.guiWelcome);
		
		this.addEventListener(eui.UIEvent.ADDED_TO_STAGE, this.init, this);
	}

    private init(): void {
		let gameControl = GameControl.getInstance();
		gameControl.alignState(this);
		gameControl.addDebugRect(this);
        
        this.guiWelcome.setCallbackBtnPlay(this.onTouchBtnPlay);
		if (this.guiWelcome.btnPlay) {
			this.guiWelcome.btnPlay.$setTouchEnabled(true);
		}
	}

    public onTouchBtnPlay(event: egret.TouchEvent): void {
        StateManager.getInstance().switchToState(GameDefine.GAME_STATE.STATE_INGAME);
    }
}