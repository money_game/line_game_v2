class StateInGame extends egret.DisplayObjectContainer {

    private guiInGame: GUIInGame;
	private guiTutorial: GUITutorial;

	private isGuiLoaded: boolean = false;

    public constructor() {
			super();

			this.guiInGame = new GUIInGame();
			this.addChild(this.guiInGame);

			this.addChild(GameManager.getInstance());
			this.addChild(SkillManager.getInstance());

			this.guiTutorial = new GUITutorial();
			this.addChild(this.guiTutorial);

			this.guiInGame.addEventListener(egret.Event.COMPLETE, this.onGuiLoaded, this);
			this.addEventTouch();
		}

		public load(): void {
			this.init();
		}

		private onGuiLoaded(): void {
			this.isGuiLoaded = true;
			this.init();
		}

		private init(): void {
			if (!this.isGuiLoaded) {
				return;
			}
			let gameControl = GameControl.getInstance();
			gameControl.alignState(this);
			gameControl.addDebugRect(this);

			GameManager.getInstance().init();
			SkillManager.getInstance().init(this.guiInGame);
			PlayerManager.getInstance().init(this.guiInGame);
		}

		public addEventTouch(): void {
			if (this.hasEventListener(egret.TouchEvent.TOUCH_TAP)) {
				this.removeEventListener(egret.TouchEvent.TOUCH_TAP, this.onTouch, this);
			}
			this.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTouch, this);
		}

		private onTouch(event: egret.TouchEvent): void {
			if (this.contains(this.guiTutorial)) {
				this.removeChild(this.guiTutorial);
			} else {
				SkillManager.getInstance().onTouch(event.stageX, event.stageY);
			}
		}
}